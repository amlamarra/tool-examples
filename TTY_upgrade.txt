# Upgrade to fully interactive TTY

# Enter while in reverse shell
python -c 'import pty; pty.spawn("/bin/bash")'

Ctrl-Z

# In Kali, make note of the rows & columns
stty raw -echo
fg

# In reverse shell
reset
export SHELL=bash
export TERM=xterm-256color
stty rows <num> columns <cols>
