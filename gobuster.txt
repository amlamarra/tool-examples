gobuster dir \
    -t 20 \
    -u http://10.10.10.121/ \
    -w /usr/share/wordlists/raft/raft-large-words.txt \
    -x $(cat extensions.txt | tr '\n' ',') \
    -o gobuster.txt
